#/bin/sh

# The purpose of this script is to use curl instead of ping
# to test for internet connectivity in shell scripts.
# This situation comes up for me because GNOME Boxes,
# Toolbx and Distrobox don't allow ping to grab packets.

if [ $? = 200 ]
	curl -sL -w "%{http_code}\\n" "http://flathub.org/" > /dev/null; then
	echo "Can connect to Flathub";
else
	echo "Cannot connect to Flathub";
fi
